module gitlab.com/gitlab-com/move-issues

go 1.14

require (
	github.com/go-resty/resty/v2 v2.3.0
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
)
