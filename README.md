# move-issues
This is a helpful command line utility that moves GitLab issues from one project to another via the GitLab REST API.

__Important details:__
* This script moves issues one by one and can take quite some time due to the request delay for each issue. *__Note:__ 
  An improvement can be made here by parallelizing the requests*
* Each move will generate an email notification to those who have notification enabled.
* The API behaviour as of the time this program was built is that moved issues will __closed__ in the source project and
  a link will be displayed that says `moved` which links to the new issues in the destination project. If an issue was
  already closed in the source project, it will remain closed in the destination project. The original issue author will
  be retained in the new issue that's created in the destination project.
  
## Usage
Download the release executable (linux) or clone and [build for your target system](#Building)
```
Usage of ./move-issues:
  -accesstoken string
    	GitLab Personal Access Token
  -destination int
    	Destination GitLab project (default -1)
  -host string
    	GitLab instance host url (default "https://gitlab.com")
  -source int
    	Source GitLab project (default -1)
```
__Arguments__:
1. __accesstoken__: Can be generated using these instructions: 
[Creating a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
1. __source and destination__: Project IDs of the projects are visible at the top of the Project Overview page
![Project ID shown underneath the project title](img/projectid.png "Project ID")
1. __host__: The GitLab host url of the projects. Defaults to `https://gitlab.com`

## Building
1. Ensure you have go 1.14
2. Clone this repo
3. Build the go module `cd move-issues && go build .`
