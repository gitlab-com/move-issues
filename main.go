package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/go-resty/resty/v2"
	"golang.org/x/sync/semaphore"
	"io"
	"log"
	"os"
	"sync"
)

func getIssuesPage(client *resty.Client, projectId int, page int) ([]map[string]interface{}, error) {
	fmt.Print(".")
	resp, err := client.R().Get(fmt.Sprintf("/projects/%d/issues?scope=all&per_page=100&page=%d", projectId, page))

	if err != nil {
		return nil, err
	}

	issues := []map[string]interface{}{}

	err = json.Unmarshal(resp.Body(), &issues)

	if err != nil {
		return nil, err
	}

	if len(issues) == 0 {
		return nil, fmt.Errorf("No more issues")
	}

	return issues, nil
}

func moveIssue(ctx context.Context, wg *sync.WaitGroup, c chan<- map[string]interface{}, sem *semaphore.Weighted, client *resty.Client, issue map[string]interface{}, srcProjectId int, dstProjectId int) {
	sem.Acquire(ctx, 1)
	log.Println("Moving issue:", int(issue["iid"].(float64)), "-", issue["title"].(string))
	defer sem.Release(1)
	defer wg.Done()
	_, err := client.R().SetFormData(map[string]string{
		"to_project_id": fmt.Sprint(dstProjectId),
	}).Post(fmt.Sprintf("/projects/%d/issues/%d/move", srcProjectId, int(issue["iid"].(float64))))

	if err != nil {
		log.Println("Error:", err)
		c <- issue
	}
}

func main() {
	f, err := os.OpenFile("log.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	mw := io.MultiWriter(os.Stdout, f)
	log.SetOutput(mw)
	
	accessToken := flag.String("accesstoken", "", "GitLab Personal Access Token")
	srcProject := flag.Int("source", -1, "Source GitLab project")
	dstProject := flag.Int("destination", -1, "Destination GitLab project")
	gitlabHost := flag.String("host", "https://gitlab.com", "GitLab instance host url")

	flag.Parse()

	if len(*accessToken) == 0 {
		log.Fatalln("GitLab Personal Access Token is required")
	}

	if *srcProject == -1 || *dstProject == -1 {
		log.Fatalln("Source and Destination arguments need to be provided")
	}

	client := resty.New()
	client.SetAuthToken(*accessToken)
	client.SetHostURL(fmt.Sprintf("%s/api/v4", *gitlabHost))

	issues := []map[string]interface{}{}
	page := 1

	// Keep fetching issues until we hit an error
	fmt.Println("Loading all issues that need to be moved")
	for err == nil {
		lissues, err := getIssuesPage(client, *srcProject, page)
		if err == nil {
			issues = append(issues, lissues...)
		} else {
			break
		}

		page++
	}

	ctx := context.TODO()
	sem := semaphore.NewWeighted(int64(100)) // To avoid tripping the GitLab API rate limiting, we use a semaphore
	c := make(chan map[string]interface{})
	wg := new(sync.WaitGroup)
	wg2 := new(sync.WaitGroup)

	// As far as I know, there's no issue batch move API endpoint so I'm just moving them one by one
	for _, issue := range issues {
		// TODO: Do this in a goroutine so it's faster
		wg.Add(1)
		go moveIssue(ctx, wg, c, sem, client, issue, *srcProject, *dstProject)
	}

	wg2.Add(1)
	go func() {
		failedIssues := []map[string]interface{}{}

		for issue := range c {
			failedIssues = append(failedIssues, issue)
		}

		if len(failedIssues) == 0 {
			fmt.Println("No failed issues")
		} else {
			fmt.Println(json.Marshal(failedIssues))
		}

		wg2.Done()
	}()

	wg.Wait()
	close(c)
	wg2.Wait()
}
